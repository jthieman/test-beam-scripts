#!/usr/bin/env python                                                                                 
import os

geodir = '/data1/TestBeam/2018_11_November_RD53_jthieman/Monicelli/Geometries/Express/'
georunnumber = '14647'

mergeddir = '/data1/TestBeam/2019_06_June_RD53/Merged/'
runnumberarray =['14679','14770','14845','14904']



for runnumber in runnumberarray:
    
    filename = open("xml/2019June_MonicelliExpress_Run"+runnumber+"_Merged.xml","w+")

    filename.write("<?xml version='1.0' encoding='iso-8859-1'?> \n")
    filename.write('<!DOCTYPE MonicelliExpressConfiguration SYSTEM "./dtd/ExpressConfiguration.dtd">\n')
    filename.write('<MonicelliExpressConfiguration>\n')
    filename.write('  <Defaults FilesPath="'+mergeddir+'"\n')
    filename.write('           GeometriesPath="'+geodir+'"\n')
    filename.write('               TrackFindingAlgorithm="FirstAndLast"\n')
    filename.write('               TrackFittingAlgorithm="Simple"\n')
    filename.write('               NumberOfEvents="50000"\n')
    filename.write('               TrackPoints="14"\n')
    filename.write('               MaxPlanePoints="-1"\n')
    filename.write('               MaxPlanePointsDUT="2"\n')
    filename.write('               Chi2Cut="-1"\n')
    filename.write('               XTolerance="500"\n')
    filename.write('               YTolerance="500"\n')
    filename.write('               XToleranceDUT="1000"\n')
    filename.write('               YToleranceDUT="1000"\n')
    filename.write('               FindDut="true"\n')
    filename.write('               RawAlignment="true"\n')
    filename.write('               FineAlignment="true"\n')
    filename.write('               FineTelescopeAlignment="true"\n')
    filename.write('               UseEtaFunction="true"/>\n')
    filename.write('   <File Name="Run'+runnumber+'_Merged.dat" Geometry="Run'+georunnumber+'_Merged.geo" />\n')
    filename.write('</MonicelliExpressConfiguration>\n')

    filename.close()


script = open("expressscript"+runnumberarray[0]+"to"+runnumberarray[len(runnumberarray)-1]+".sh","w+")

for runnumber in runnumberarray:
    script.write('nohup ./MonicelliExpress 2019June_MonicelliExpress_Run'+runnumber+'_Merged.xml &> ExpressLogs/2019June_Run'+runnumber+'_Merged.log \n')
script.close()