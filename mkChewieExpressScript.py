#!/usr/bin/env python                                                                                 
import os

runnumberarray =['14679','14770','14845','14904']

for runnumber in runnumberarray:
    
    filename = open("xml/2019June_ChewieExpress_Run"+runnumber+"_Merged.xml","w+")

    filename.write("<?xml version='1.0' encoding='iso-8859-1'?> \n")
    filename.write('<!DOCTYPE ChewieExpressConfiguration SYSTEM "./dtd/ExpressConfiguration.dtd"> \n')
    filename.write('<ChewieExpressConfiguration> \n')
    filename.write('    <Defaults FilesPath="../../Monicelli/MonicelliOutput/" \n')
    filename.write('              Convert="yes" \n')
    filename.write('              RunAnalysis="yes" \n')
    filename.write('              NumberOfEvents="-1"/> \n')
    filename.write('      <Files Configuration="ChewieConfiguration_50x50.xml" OutFileName="Chewie_Run'+runnumber+'.root"> \n')
    filename.write('      <File Name="Run'+runnumber+'_Merged.root"/> \n')
    filename.write('    </Files> \n')
    filename.write('</ChewieExpressConfiguration> \n')

    filename.close()


script = open("expressscript"+runnumberarray[0]+"to"+runnumberarray[len(runnumberarray)-1]+".sh","w+")

for runnumber in runnumberarray:
    script.write('nohup ./ChewieExpress 2019June_ChewieExpress_Run'+runnumber+'_Merged.xml &> ExpressLogs/2019June_Run'+runnumber+'_Merged.log \n')
script.close()
